Sparkler Particle System Project

This is a program written in C with the use of OpenGL to simulate a sparkler particle system made as part of Advanced Graphics module of University of Manchester.

You can read the report in Report_Daria.pdf for details of decision making, controls and other comments.
You can watch Graphics_Particle_System_Daria.mp4 for a demonstration of simulation and the program's features.
