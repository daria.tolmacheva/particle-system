#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Shader.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <iostream>
#include <stdio.h>
#include <vector>


void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xPos, double yPos);
//void scroll_callback(GLFWwindow* window, double xOffset, double yOffset);
void processInput(GLFWwindow* window);

// Display list for coordinate axis
GLuint axisList;

#define SCREEN_WIDTH 1280
#define SCREEN_HIGHT 960
#define TIME_STEP 0.000000000004


///////////////////////////////////////////////

float myRandom()
//Return random double within range [-1,1]
{
    int initRand = rand();
    if (initRand % 2 == 0)
        return (initRand/(float)RAND_MAX);
    else
        return -(initRand/(float)RAND_MAX);
}


bool firstMouse = true;
float yaw = -90.0f;    // yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
float pitch = 0.0f;
float lastX = 800.0f / 2.0;
float lastY = 600.0 / 2.0;
float fov = 45.0f;

//time
float  timeDiff = 0.0f;
float  lastFrame = 0.0f;

//camera
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

float maxVelocity;
float maxDeceleration;
float maxGravity;
float maxLifetime;
unsigned int numberOfParticles;
unsigned int numberOfSources;

struct particle {
    glm::vec3 position, color, velocity, acceleration;
    float lifetime;

    particle()
        : position(0.0f, 0.0f, 0.0f),
          color(255.0f, 255.0f, 255.0f),
          velocity(maxVelocity * myRandom(), maxVelocity * myRandom(), maxVelocity * myRandom()),
          acceleration(maxDeceleration * this->velocity[0], maxGravity + maxDeceleration * this->velocity[1], maxDeceleration * this->velocity[2]),
          lifetime(maxLifetime * myRandom()) {}

};

//particle particles[10];
std::vector<particle> particles;
unsigned int particleOneVBO, particleOneVAO;
std::vector<glm::vec3> particlesPositions;


//particle movements
double currentTime = glfwGetTime();


void updateParticle()
{
    double timePassed = glfwGetTime();
    if ((timePassed - currentTime) > 0.1)
    {
        for (unsigned int i = 0; i < particles.size(); i++)
        {
            //update lifetime
            particles[i].lifetime -= (timePassed - currentTime);
            
            if (particles[i].lifetime < 0.0f) {
                //respawn the particle
                //reset the location
                if (numberOfSources == 2) {
                    if (i % 2 == 0) {
                        particles[i].position.x = 0.0f;
                        particles[i].position.y = 0.0f;
                        particles[i].position.z = 0.0f;
                    } else {
                        particles[i].position.x = 0.0f;
                        particles[i].position.y = 0.0f;
                        particles[i].position.z = 0.0f;
                    }
                } else {
                    particles[i].position.x = 0.0f;
                    particles[i].position.y = 0.0f;
                    particles[i].position.z = 0.0f;
                }
                //reset the velocity
                particles[i].velocity.x = maxVelocity * myRandom();
                particles[i].velocity.y = maxVelocity * myRandom();
                particles[i].velocity.z = maxVelocity * myRandom();
                //reset acceleration
                particles[i].acceleration.x = maxDeceleration * particles[i].velocity.x;
                particles[i].acceleration.y = maxGravity + maxDeceleration * particles[i].velocity.y;
                particles[i].acceleration.z = maxDeceleration * particles[i].velocity.z;
                //reset lifetime
                particles[i].lifetime = maxLifetime * myRandom();
                
            } else {
                //change velocity based on acceleration
                particles[i].velocity.x += particles[i].acceleration.x;
                particles[i].velocity.y += particles[i].acceleration.y;
                particles[i].velocity.z += particles[i].acceleration.z;
                //change position based on velocity
                particles[i].position.x += particles[i].velocity.x;
                particles[i].position.y += particles[i].velocity.y;
                particles[i].position.z += particles[i].velocity.z;
            
                //update offsets/positions buffer
                particlesPositions[i] = particles[i].position;
            }
        }
        //update current time
        currentTime = glfwGetTime();
    }

}


int main(void)
{
    
    //initialize particle/world properties
    maxVelocity = 0.05f;
    maxDeceleration = -0.01f;
    maxGravity = -0.000001;
    maxLifetime = 1.5f;
    numberOfParticles = 1000;
    numberOfSources = 1;
    
    
    /* Initialize the library */
    if (!glfwInit())
        return -1;

    GLFWwindow* window;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE );

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HIGHT, "Particle System", NULL, NULL);
    if (!window)
    {
        std::cout << "Failed to create glfw window" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    //glfwSetScrollCallback(window, scroll_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    if ( GLEW_OK != glewInit( ) )
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return EXIT_FAILURE;
    }


    glEnable(GL_DEPTH_TEST);


    // build and compile our shader zprogram
    // ------------------------------------
    Shader ourShader("resourses/shaders/shader.vs", "resourses/shaders/shader.fs");
    
    particles.resize(numberOfParticles);
    particlesPositions.resize(numberOfParticles);

    float particleOne[] = {
        //position        //color                 //velocity        //acceleration
        0.0f, 0.0f, 0.0f, 255.0f, 255.0f, 255.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };


    for (unsigned int i=0; i < particles.size(); i++)
    {
        //initialize offsets/positions buffer
        particlesPositions[i] = particles[i].position;
    }

//    unsigned int instanceVBO;
//    glGenBuffers(1, &instanceVBO);
//    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
//    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 10, &particlesPositions[0], GL_STATIC_DRAW);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);



    glGenVertexArrays(1, &particleOneVAO);
    glGenBuffers(1, &particleOneVBO);
    glBindVertexArray(particleOneVAO);
    glBindBuffer(GL_ARRAY_BUFFER, particleOneVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(particleOne), particleOne, GL_STATIC_DRAW);

    //position
    //glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    //color
    //glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
    //individual offset
//    glEnableVertexAttribArray(2);
//    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO); // this attribute comes from a different vertex buffer
//    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
//    glBindBuffer(GL_ARRAY_BUFFER, 0);
//    glVertexAttribDivisor(2, 1); // tell OpenGL this is an instanced vertex attribute.

    
    //load sprite
    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    //set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // load image, create texture and generate mipmaps
    int width, height, nrChannels;
    stbi_set_flip_vertically_on_load(1); // tell stb_image.h to flip loaded texture's on the y-axis.
    unsigned char* data = stbi_load("resourses/images/spark.png", &width, &height, &nrChannels, 4);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);
    
    ourShader.use();
    ourShader.setInt("texture", 0);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        //interframe time change
        float currentFrame = glfwGetTime();
        timeDiff = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window);

        //render here
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glBindTexture(GL_TEXTURE_2D, texture);

        particles.resize(numberOfParticles);
        particlesPositions.resize(numberOfParticles);
        
        for (unsigned int i = 0; i < particles.size(); i++)
        {
            //initialize offsets/positions buffer
            particlesPositions[i] = particles[i].position;
        }

        //    unsigned int instanceVBO;
        //    glGenBuffers(1, &instanceVBO);
        //    glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
        //    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 10, &particlesPositions[0], GL_STATIC_DRAW);
        //    glBindBuffer(GL_ARRAY_BUFFER, 0);



        glGenVertexArrays(1, &particleOneVAO);
        glGenBuffers(1, &particleOneVBO);
        glBindVertexArray(particleOneVAO);
        glBindBuffer(GL_ARRAY_BUFFER, particleOneVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(particleOne), particleOne, GL_STATIC_DRAW);

        //position
        //glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)0);
        glEnableVertexAttribArray(0);
        //color
        //glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float), (void*)(3*sizeof(float)));
        glEnableVertexAttribArray(1);


        //shader
        ourShader.use();

        //ptojection matrix for shader
        glm::mat4 projection = glm::perspective(glm::radians(fov), ((float)SCREEN_WIDTH/(float)(SCREEN_HIGHT)), 1.0f, 100.0f);
        ourShader.setMat4("projection", projection);
        //view(camera) matrix for shader
        glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        ourShader.setMat4("view", view);

        //render particles
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
        glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
        glEnable(GL_POINT_SPRITE);
        glBindVertexArray(particleOneVAO);
        for (unsigned int i = 0; i < particlesPositions.size(); i++)
        {
            //std::cout << particlePositions[i] << std::endl;
            //model matrix for the shader
            glm::mat4 model = glm::mat4(1.0f);
            model = glm::translate(model, particlesPositions[i]);
            //float angle = 20.0f*i;
            //model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
            ourShader.setMat4("model", model);
            glPointSize(25);
            glDrawArrays(GL_POINTS, 0, 3);
        }

//        glGenBuffers(1, &instanceVBO);
//        glBindBuffer(GL_ARRAY_BUFFER, instanceVBO);
//        glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 10, &particlesPositions[0], GL_STATIC_DRAW);
//        glBindBuffer(GL_ARRAY_BUFFER, 0);

//        glBindVertexArray(particleOneVAO);
//        glDrawArraysInstanced(GL_POINTS, 0, 3, 100); // 100 POINTS of 1 vertices each
//        glBindVertexArray(0);


        updateParticle();
        glDisable(GL_BLEND);

        /* Swap front and back buffers */
        glfwSwapBuffers(window);

        /* Poll for and process events */
        glfwPollEvents();
    }

    glDeleteVertexArrays(1, &particleOneVAO);
    glDeleteBuffers(1, &particleOneVBO);

    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    
    float cameraSpeed = 2 * timeDiff;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        cameraPos += cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        cameraPos -= cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    
    //restart particles
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    {
        for (unsigned int i = 0; i < numberOfParticles; i++)
        {
            // move the particle down depending on the velocity
            particles[i].position.x = 0.0f;
            particles[i].position.y = 0.0f;
            particles[i].position.z = 0.0f;
            //reset the velocity
            particles[i].velocity.x = maxVelocity * myRandom();
            particles[i].velocity.y = maxVelocity * myRandom();
            particles[i].velocity.z = maxVelocity * myRandom();
            //reset acceleration
            particles[i].acceleration.x = maxDeceleration * particles[i].velocity.x;
            particles[i].acceleration.y = maxGravity + maxDeceleration * particles[i].velocity.y;
            particles[i].acceleration.z = maxDeceleration * particles[i].velocity.z;
            //reset lifetime
            particles[i].lifetime = maxLifetime * myRandom();
        }
    }
    
    //reset parameters R
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
    {
        maxVelocity = 0.05f;
        maxDeceleration = -0.01f;
        maxGravity = -0.000001f;
        maxLifetime = 1.5f;
        numberOfParticles = 15;
        numberOfSources = 1;
    }
    //change lifetime
    //increase lifetime X
    if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS)
        maxLifetime += 0.5f;
    //decrease lifetime Z
    if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS){
        if (maxLifetime > 0.0f)
            maxLifetime -= 0.5f;
    }
    
    //change velocity
    //increase velocity V
    if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
        maxVelocity += 0.001f;
    //decrease velocity C
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
        if (maxVelocity > 0.0f)
            maxVelocity -= 0.001f;
    }
    
    //change gravity
    //increase gravity N
    if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
        maxGravity -= 0.0001f;
    //decrease gravity B
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS){
        if (maxGravity < 0.0f)
            maxGravity += 0.0001f;
    }
    
    //change deceleration
    //increase deceleration L
    if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
        maxDeceleration -= 0.0005f;
    //decrease deceleration K
    if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS){
        if (maxDeceleration < 0.0f)
            maxDeceleration += 0.0005f;
    }
    
    //change number of particles
    //increase number of particles 0
    if (glfwGetKey(window, GLFW_KEY_0) == GLFW_PRESS)
        numberOfParticles += 5;
    //decrease number of particles 9
    if (glfwGetKey(window, GLFW_KEY_9) == GLFW_PRESS){
        if (numberOfParticles > 0)
            numberOfParticles -= 5;
    }
    
    //change number of sources
    //one sourse 1
//    if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS) {
//        if (numberOfSources == 2) {
//            numberOfParticles = numberOfParticles/2;
//            numberOfSources = 1;
//        }
//    }
//    //two sources 2
//    if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS){
//        if (numberOfSources == 1) {
//            numberOfParticles = numberOfParticles * 2;
//            numberOfSources = 2;
//        }
//    }
    
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        if (fov < 90.0f)
            fov += 1.0f;
    }
    //decrease number of particles 9
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        if (fov > 1.0f)
            fov -= 1.0f;
    }
    
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow* window, double xPos, double yPos)
{
    if (firstMouse)
    {
        lastX = xPos;
        lastY = yPos;
        firstMouse = false;
    }
    
    float xoffset = xPos - lastX;
    float yoffset = lastY - yPos; // reversed since y-coordinates go from bottom to top
    lastX = xPos;
    lastY = yPos;
    
    float sensitivity = 0.1f; // change this value to your liking
    xoffset *= sensitivity;
    yoffset *= sensitivity;
    
    yaw += xoffset;
    pitch += yoffset;
    
    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;
    
    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(front);
}

//void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
//{
//    fov -= (float)yOffset;
//    if(fov < 1.0f)
//        fov = 1.0f;
//    if(fov > 45.0f)
//        fov = 45.0f;
//}
