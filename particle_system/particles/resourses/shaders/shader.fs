#version 330 core
out vec4 FragColor;

in vec3 ourColor;

uniform sampler2D sprite;

void main()
{
    vec2 tCoords = vec2(gl_PointCoord.x, gl_PointCoord.y);
    FragColor = texture(sprite, tCoords);
}

